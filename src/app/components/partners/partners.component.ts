import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'; 
import { PageShareService } from '../../services/page-share.service';

@Component({
    selector: 'partners',
    templateUrl: './partners.component.html',
    styleUrls: ['./partners.component.sass']
})
export class PartnersComponent implements OnInit {

    pageState: String;
    constructor(
        router: Router,
        actRouter: ActivatedRoute,
        public pageService: PageShareService,
    ){
        this.pageState = actRouter.data['value'].page;
    }
    ngOnInit() {
        this.pageService.changePage(this.pageState);;
        this.pageState = this.pageService.get();      
    }

}
