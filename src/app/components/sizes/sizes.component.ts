import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'sizes',
    templateUrl: './sizes.component.html',
    styleUrls: ['./sizes.component.sass']
})
export class SizesComponent implements OnInit {

    @Input() activatedSizes:Array<any>;
    @Output('update') changeActivatedSizes:EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

    public sizes = [ 
        {
            name: '37',
            active: false
        },
        {
            name: '38',
            active: false
        },
        {
            name: '39',
            active: false
        },
        {
            name: '40',
            active: false
        },
        {
            name: '41',
            active: false
        },
        {
            name: '42',
            active: false
        },                                        
        {
            name: '43',
            active: false
        },        
        {
            name: '44',
            active: false
        },        
        {
            name: '45',
            active: false
        },        
        {
            name: '46',
            active: false
        },        
        {
            name: '47',
            active: false
        }
    ];
    constructor() { }

    ngOnInit() {
    }

    public activate(size){
        size['active'] = size['active'] ? false : true; 
        if(size['active']){ 
            this.activatedSizes.push(size);
        }
        else{
            let index = this.activatedSizes.indexOf(size);
            if(index > -1){
                this.activatedSizes.splice(index, 1);
            }
        }
        this.changeActivatedSizes.emit(this.activatedSizes);
    }
}
