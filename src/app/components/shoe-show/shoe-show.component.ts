import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { HelperService } from '../../services/helper.service';
import { Router, NavigationExtras, RoutesRecognized } from "@angular/router";
import { routerTransition } from '../../animations/routerTransition';
import { BusketService } from '../../services/busket.service';
import { Order } from '../../models/order';
import { tableSizes } from '../../models/table-sizes';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'shoe-show',
    templateUrl: './shoe-show.component.html',
    styleUrls: ['./shoe-show.component.sass'],
    animations: [routerTransition],    
})

export class ShoeShowComponent implements OnInit {

    public id:number;
    public el;
    public indexes:any = [];
    public next:number;
    public last:number;
    public Object = Object;
    public openedParams:boolean = false;
    public markers:Array<Object> = [];
    public openTable:boolean = false;
    public isAdding:boolean = false;
    public host = environment.host;
    public zoomStyles = {
        'transform-origin': '0px 0px',
    }

    public activeImage:string;

    constructor(
        private route: ActivatedRoute,
        private api: ApiService,
        public _: HelperService,
        private router: Router,
        public busketService: BusketService,
    ) { }

    public smallSliderConfig = {
        arrows: false,
        dots: false,
        slidesToShow: 3,
        slidsToScroll: 1,
        centerMode: true,
        focusOnSelect: true,        
    }

    public mobileSmallSliderConfig = {
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidsToScroll: 1,
        centerMode: false,        
    }    

    public activateColor(color){
        this.el.activeColor = color;
    }

    public activateSize(size){
        this.el.activeSize = size;
    }
    
    public afterChange(e){
        let index = e.currentSlide;
        this.activeImage = this.el.images[index];
    }

    public navigate(id, side){
        let navigationExtras: NavigationExtras = {
            queryParams: {
                side: side 
            }
        };            
        this.router.navigate(["/catalog", id], navigationExtras);
    }

    public getState() {
        return this.route.queryParams['value']['side'];
    }    

    public except:boolean = false;
    public deviceWidth:number;

    ngOnInit() {
        this.deviceWidth = window.innerWidth;

        this.route.params.subscribe(params => {
            this.openedParams = false;
            this.id = parseInt(params.id);

            this.api.get('/api/indexes').subscribe(
                res => {
                    this.indexes = res;
                    let curInd = this.indexes.indexOf(this.id);

                    this.last = this.indexes[curInd - 1];
                    this.next = this.indexes[curInd + 1];

                    if(curInd == this.indexes.length - 1){
                        this.next = this.indexes[0];
                    }
                    if(curInd == 0){

                        this.last = this.indexes[this.indexes.length - 1];

                    }

                }
            );

            this.route.queryParams.subscribe(qParams => {
                if(this._.isEmpty(qParams['el'])){
                    this.api.get('/api/shoes/'+this.id).subscribe(
                        res => {
                            this.el = res;
                            this.el.activeColor = res['colors'][0]
                            this.el.activeSize = res['sizes'][0];
                            this.activeImage = this.el.images[0];
                            this.marked(this.el);
                            console.log(this.el);
                        },
                        err => {
                            if(err.status == 404){
                                this.except = true;
                            }
                        }
                    );
                }else{
                    this.el = JSON.parse(qParams.el);
                    if(!this.el.activeColor){
                        this.el.activeColor = this.el['colors'][0]
                    }
                    if(!this.el.activeSize){
                        this.el.activeSize  = this.el['sizes'][0];
                    }
                    this.activeImage = this.el.images[0];   
                    console.log(this.el);
                    this.marked(this.el);

                }

            });

        });

    }

    public addToCart(){
        this.isAdding = true;
        setTimeout(()=>{
            this.isAdding = false;
        }, 3000)
        this.busketService.addOrder(this.el);
    }

    public oneClickBuy(){
        this.busketService.setQuickOrder(this.el);
        this.router.navigate(['/ordering'])
    }

    public marked(el){
        this.markers = [];
        if(el['isNew']){
            this.markers.push({
                type: 'new',
                text: 'NEW'
            });
        }
        if(el['isSale']){
            this.markers.push({
                type: 'sales',
                text: '-'+this.el['sale'] + '%' 
            });
        }
        if(el['popular']){
            this.markers.push({
                type: 'popular',
                text: 'TOP'
            });
        }              
    }
    public changeState(){
        this.openedParams = !this.openedParams;
    }

    public zoomable(e){
        let x = e.offsetX;
        let y = e.offsetY;
        let styles = x + 'px ' + y + 'px'

        this.zoomStyles['transform-origin'] = styles;
    }

}
