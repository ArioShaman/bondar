import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'team-slider',
    templateUrl: './team-slider.component.html',
    styleUrls: ['./team-slider.component.sass']
})
export class TeamSliderComponent implements OnInit {
    public deviceWidth:number = 0;
    public slides = [
        {
            img: '../../assets/images/vova.jpg',
            mobImg: '../../assets/images/vova.jpg',
            position: 'Директор',
            name: 'Владимир',
            age: '23 года',
            desc: 'Наши дорогие клиенты! Меня зовут Владимир Бондарев. Я явлюсь Директором компании "БондарЪ". Моя команда создала проект,где каждый уважающий себя мужчина сможет купить стильную и качественную обувь. Девушки смотрят в первую очередь на 2 вещи: лицо и обувь,так что Друзья - будьте привлекательны и уверенны в себе! Мы Вам в этом обязательно поможем😉'
        },
        {
            img: '../../assets/images/slava.jpg',
            mobImg: '../../assets/images/slava.jpg',
            position: 'Главный менеджер',
            name: 'Вячеслав',
            age: '21 год',
            desc: 'Друзья,меня зовут Вячеслав и Я являюсь главным менеджером компании "БондарЪ". Моя основная задача - обеспечить оформление Вашего заказа и своевременной доставки товара. Мы ставим Ваше время и эмоции на первое место, как говорится: "дорога ложка к обеду". Компания "БондарЪ" рада каждому новому и "бывалому" клиенту, а Я с большим удовольствием помогу Вам с выбором товара и дальнейшими действиями)'
        },
    ];

    public slideConfig = {
        slidesToShow: 1,
        dots: true,
        slidesToScroll: 1,  
        arrow: true,
        prevArrow: '<div class="arrow-cont left"><div class="arrow left-arrow"><img src="../../assets/icons/left.svg" alt="arrow-left" draggable="false"></div><img draggable="false" src="../../assets/images/blur-left.png" alt="blur"></div>', 
        nextArrow: '<div class="arrow-cont right"><div class="arrow right-arrow"><img src="../../assets/icons/right.svg" alt="arrow-left" draggable="false"></div><img draggable="false" src="../../assets/images/blur-right.png" alt="blur"></div>',
    }
    constructor() { }

    ngOnInit() {
        this.deviceWidth = window.innerWidth;
    }

}
