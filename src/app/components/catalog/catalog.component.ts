import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'; 
import { PageShareService } from '../../services/page-share.service';
// import { shoesList } from '../../models/shoes-list';
import { ApiService } from '../../services/api.service';
import { AngularTokenService } from "angular-token";
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'app-catalog',
    templateUrl: './catalog.component.html',
    styleUrls: ['./catalog.component.sass']
})
export class CatalogComponent implements OnInit {
    pageState: String;
    public openFilters:boolean = false;
    public yetLoading = false; 
    public Math: any;
    public opened: boolean = false;
    public fixed:boolean = false;
    public navHeight:number = 0;
    public shoesList:any = [];//shoesList;
    public loaddedList:Array<Object> = [];
    public lastIndex = 8;
    public isDeleting:boolean = false;

    public priceValue = {
        minValue: 1000,
        maxValue: 10000
    };

    public filterArgs:Array<Object> = [
        {
            type: 'color',
            value: []
        },
        {
            type: 'size',
            value: []
        },
        {
            type: 'minPrice',
            value: this.priceValue['minValue']
        },
        {
            type: 'maxPrice',
            value : this.priceValue['maxValue']
        }

    ]; 
    public activatedColors:Array<any> = [];
    public activatedSizes:Array<any> = [];

    public new:boolean =  false;
    public sales:boolean = false;
    public popular:boolean = false;


    public orderBy:string = '';
    public categories = [
        {
            name: 'Кеды',
            type: 'sneakers',
            value: false
        },
        {
            name: 'Кросcовки',
            type: 'gym shoe',
            value: false
        },     
        {
            name: 'Туфли',
            type: 'low shoe',
            value: false
        },            
    ];

    public orderArgs:Array<any> = [
        {
            type: 'isNew',
            value: this.new,
        },
        {
            type: 'isSale',
            value: this.sales,
        },        
        {
            type: 'popular',
            value: this.popular,
        },   
        {
            type: 'sneakers',
            value: this.categories[0]['value'],
        },  
        {
            type: 'gym shoe',
            value: this.categories[1]['value'],
        },  
        {
            type: 'low shoe',
            value: this.categories[2]['value'],
        },                                
    ];

    constructor(
        router: Router,
        actRouter: ActivatedRoute,
        public pageService: PageShareService,
        private api: ApiService,
        public tokenAuthService:AngularTokenService,
    ){
        this.pageState = actRouter.data['value'].page;
        this.Math = Math;
    }

    ngOnInit() {

        this.api.get('/api/shoes').subscribe(
            res => {
                this.shoesList = res;
                this.loaddedList = this.shoesList.slice(0,8);
            }
        );


        this.pageService.changePage(this.pageState);
        this.pageState = this.pageService.get();  

        this.navHeight = document.querySelector('.background').clientHeight;
        window.addEventListener('scroll', () => {
            let curScrollPos =  window.pageYOffset;
            if(curScrollPos > this.navHeight){
                this.fixed = true;
            }else{
                this.fixed = false;
            }
        });                
    }

    public loadShoes(){
        this.yetLoading = true;
        setTimeout(()=>{
            let newList = this.shoesList.slice(this.lastIndex, this.lastIndex + 8);
            this.loaddedList = this.loaddedList.concat(newList);
            this.lastIndex += 8;

            this.yetLoading = false;
        }, 1000)

    }
    public activateState(e){

        switch (e) {
            case "new":
                this.new = !this.new;
                this.orderArgs[0]['value'] = !this.orderArgs[0]['value'];
                break;
            case "sales":
                this.sales = !this.sales;
                this.orderArgs[1]['value'] = !this.orderArgs[0]['value'];
                break;
            case "popular":
                this.popular = !this.popular;
                this.orderArgs[2]['value'] = !this.orderArgs[0]['value'];
                break;        
        }
    }

    public handleState(e, state) {
        
        switch (state) {
            case "new":
                this.new = e;
                this.orderArgs[0]['value'] = e;
                break;
            case "sales":
                this.sales = e;
                this.orderArgs[1]['value'] = e;
                break;
            case "popular":
                this.popular = e;
                this.orderArgs[2]['value'] = e;
                break;
            case "sneakers":           
                this.categories[0]['value'] = e;
                this.orderArgs[3]['value'] = e;

                this.categories[1]['value'] = false;
                this.orderArgs[4]['value'] = false; 
                this.categories[2]['value'] = false;
                this.orderArgs[5]['value'] = false;                                
                break;
            case "gym shoe":            
                this.categories[1]['value'] = e;
                this.orderArgs[4]['value'] = e;

                this.categories[0]['value'] = false;
                this.orderArgs[3]['value'] = false; 
                this.categories[2]['value'] = false;
                this.orderArgs[5]['value'] = false;  

                break; 
            case "low shoe":
                this.categories[2]['value'] = e;
                this.orderArgs[5]['value'] = e;

                this.categories[1]['value'] = false;
                this.orderArgs[4]['value'] = false; 
                this.categories[0]['value'] = false;
                this.orderArgs[3]['value'] = false;  
                break;                                
        }
    }

    public handleColors(e){
        this.activatedColors = e;
        this.filterArgs[0]['value'] = e;
    }
    public handleSizes(e){
        this.activatedSizes = e;
        this.filterArgs[1]['value'] = e;
    }    
    public handlePrice(e){
        this.priceValue = e;
        this.filterArgs[2]['value'] = e['minValue'];
        this.filterArgs[3]['value'] = e['maxValue'];
    }

    public open(){
        this.opened = true;
    }
    public chooseOrder(order:string){
        this.orderBy = order;
        this.opened = false;
    }

    public resetFilters(){
        for(let order of this.orderArgs){
            order['value'] = false;
        }

        this.new = false;
        this.sales = false;
        this.popular = false;
        this.categories[0]['value'] = false;
        this.categories[1]['value'] = false;

        for(let color of this.activatedColors){
            color.active = false;
        }
        this.activatedColors = this.filterArgs[0]['value'] = [];

        for(let size of this.activatedSizes){
            size.active = false;
        }
        this.activatedSizes = this.filterArgs[1]['value'] = [];

        this.priceValue.minValue =this.filterArgs[2]['value'] = 1000;
        this.priceValue.maxValue = this.filterArgs[3]['value'] = 10000;
    }

    public deletedId = 0;
    public callModal(e){
        this.deletedId = e;
        this.isDeleting = true;
    }

    public deleteShoe(){
        this.api.delete('/api/shoes/'+ this.deletedId).subscribe(
            res => {
                this.isDeleting = false;
                this.updateList();
            }
        );
    }

    public updateList(){
        this.api.get('/api/shoes').subscribe(
            res => {
                this.shoesList = res;
                this.loaddedList = this.shoesList.slice(0, this.lastIndex);
            }
        );        
    }
}

