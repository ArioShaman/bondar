import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'; 
import { PageShareService } from '../../services/page-share.service';
import { ApiService } from '../../services/api.service';
import { environment } from '../../../environments/environment';
import {VgAPI} from 'videogular2/core';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

    @ViewChild('miniCatalog') miniCatalog:ElementRef;
    @ViewChild('media') media:ElementRef;

    pageState: String;
    public shoesList:any;
    public activeShoe;
    public sliderConfig = {
        arrows: false,
        dots: false,
        slidesToShow: 2,
        slidsToScroll: 1,
        centerMode: true,
        focusOnSelect: true,        
        speed: 400
    }
    public activeEl;
    public host = environment.host;
    constructor(
        public router: Router,
        public actRouter: ActivatedRoute,
        public pageService: PageShareService,
        public api: ApiService,     
    ){
        this.pageState = actRouter.data['value'].page;
    }
    ngOnInit() {
        console.log(this.media);
        this.api.get('/api/shoes').subscribe(
            res => {
                console.log(res);
                this.shoesList = res;
                this.activeShoe = this.shoesList[0];               
            }
        );        
        this.pageService.changePage(this.pageState);;
        this.pageState = this.pageService.get();
    }

    public scroll(){
        this.miniCatalog.nativeElement.scrollIntoView({
            behavior:"smooth"
        })
    }

    public afterChange(e){
        let index = e.currentSlide;
        this.activeShoe = this.shoesList[index];
    }
}
