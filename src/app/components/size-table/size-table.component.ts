import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { tableSizes } from '../../models/table-sizes';

@Component({
    selector: 'size-table',
    templateUrl: './size-table.component.html',
    styleUrls: ['./size-table.component.sass']
})
export class SizeTableComponent implements OnInit {

    public tableSizes:Array<Object> = tableSizes;
    @Input() openTable:boolean = false;
    @Output() detectClose:EventEmitter<boolean> = new EventEmitter<boolean>();
    constructor() { }

    ngOnInit() {
    }

    public openTableAction(){
        this.openTable = true;
    }
    public closeTableAction(){
        this.openTable = false;
        this.detectClose.emit(true);
    }    
}
