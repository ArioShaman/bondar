import { 
    Component, 
    OnInit, 
    ViewChild, 
    ElementRef, 
    Input, 
    Output,
    EventEmitter
} from '@angular/core';

@Component({
    selector: 'binput',
    templateUrl: './bondar-input.component.html',
    styleUrls: ['./bondar-input.component.sass']
})
export class BondarInputComponent implements OnInit {
    
    @ViewChild("myInp") inputEl: ElementRef;
    
    @Input('placeholder') placeholder:string;
    @Input('type') type:string;
    @Input('name') name:string;
    @Input('field') field:string;
    @Input('fontSize') fontSize:string = '18';
    
    @Input('data') data:string;
    
    @Output() dataChange:EventEmitter<Object> = new EventEmitter<Object>();
    @Output() handleEnter:EventEmitter<String> = new EventEmitter<String>();

    public isFocused:boolean = false;

    constructor(
        private el: ElementRef
    ) { }


    ngOnInit() {
    }

    ngDoCheck(){
        if(this.data[this.field].length > 0){
            this.isFocused = true;
        }        
    }

    public focusInput(){
        this.isFocused = true;
        this.inputEl.nativeElement.focus(document.querySelector('input'));
    }
    public unfocus(){
        if(this.data[this.field].length == 0){
            this.isFocused = false;
        }
    }
    public dataChanged(e){
        this.dataChange.emit(this.data);
    }
    public enter(){
        this.isFocused = false;
        this.inputEl.nativeElement.blur(document.querySelector('input'));
        this.handleEnter.emit('entered');
    }
}
