import { Component, OnInit } from '@angular/core';
import { BusketService } from '../../services/busket.service';
import { FormBuilder, FormControl,Validators, FormGroup } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { Router, NavigationExtras, RoutesRecognized, ActivatedRoute } from '@angular/router'; 


@Component({
    selector: 'order-page',
    templateUrl: './order-page.component.html',
    styleUrls: ['./order-page.component.sass']
})


export class OrderPageComponent implements OnInit {

    public isUpdated:boolean = false;
    public complexForm : FormGroup;
    public openByclick:boolean = false;

    constructor(
        public busketService:BusketService,
        public router:Router,
        private api: ApiService,
        public fb: FormBuilder, 
    ) { 
        this.complexForm = fb.group({
            'last_name': ['', Validators.required ],
            'first_name': ['', Validators.required ],
            'middle_name': ['', Validators.required ],
            'post_index': ['', Validators.required ],
            'region': ['', Validators.required ],  
            'city': ['', Validators.required ],                        
            'street_data': ['', Validators.required ],
            'flat_data': [''],
            'delivery_way': ['', Validators.required ],
            'phone': ['', Validators.required ],
            'email': ['', Validators.required ],
        });
    }

    public redirect(){
        this.isUpdated = false;
        this.router.navigate(['/catalog']);
    }

    public onSubmit(complexForm){
        console.log(complexForm);
        let orders:Array<any>;
        if(this.openByclick){
            orders = [this.busketService.quickOrder];
        }else{
            orders = this.busketService.get()
        }
        this.api.post("/api/order", {orderData: complexForm, orders: orders}).subscribe(
            res => {
                this.isUpdated = true;
                if(!this.openByclick){
                    this.busketService.clear();
                }
            },
            err => {
                console.log(err);
            }
        );
    }

    ngOnInit() {
        if(this.busketService.get().length == 0 && this.busketService.getIsQuick() == false){
            this.router.navigate(['/catalog']);
        }
        if(this.busketService.getIsQuick()){
            this.openByclick = true;
        }
        this.busketService.clearQuick();
    }

}
