import { Component, OnInit } from '@angular/core';
import { PageShareService } from '../../services/page-share.service';
import { Router, NavigationExtras, RoutesRecognized, ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../services/auth.service'
import { AngularTokenService } from 'angular-token';
import { ApiService } from '../../services/api.service';

@Component({
    selector: 'profile-page',
    templateUrl: './profile-page.component.html',
    styleUrls: ['./profile-page.component.sass']
})
export class ProfilePageComponent implements OnInit {
    pageState: String;
    constructor(
        public pageService: PageShareService,
        public actRouter: ActivatedRoute,
        public tokenService: AngularTokenService,
        public router: Router,
        public authService: AuthService  
    ) { 
        this.pageState = actRouter.data['value'].page;
        this.pageService.changePage(this.pageState);
    }

    ngOnInit() {
        if(this.tokenService.userSignedIn()){
            console.log('in');
        }else{
            this.router.navigate(['/logIn']);
        }        
    }

    public logOut(){
        console.log('click');
        this.authService.logOutUser().subscribe(() => this.router.navigate(['/']));
    }
}
