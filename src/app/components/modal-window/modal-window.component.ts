import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'modal-window',
    templateUrl: './modal-window.component.html',
    styleUrls: ['./modal-window.component.sass']
})
export class ModalWindowComponent implements OnInit {
    @Input() text:string;
    @Input() open:boolean;
    @Output() accepted = new EventEmitter<boolean>();
    @Output() canceled = new EventEmitter<boolean>();

    constructor() { }

    ngOnInit() {
    }

    public no(){
        this.open = false;
        this.canceled.emit(false);
    }
    public yes(){
        this.open = false;
        this.accepted.emit(true);
    }
}
