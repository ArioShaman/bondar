import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ctrl-check-box',
    templateUrl: './controll-check-box.component.html',
    styleUrls: ['./controll-check-box.component.sass']
})
export class ControllCheckBoxComponent implements OnInit {
    @Input() state;
    @Output('update') changeState:EventEmitter<Object>  = new EventEmitter<Object>();
    @Input('field') field:string;
    constructor() { }

    ngOnInit() {

    }

    public change(){
        this.state[this.field].value = this.state[this.field].value ? false : true;
        // console.log(this.state);
        this.changeState.emit(this.state);
    }


}
