import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'dialog-window',
    templateUrl: './dialog-window.component.html',
    styleUrls: ['./dialog-window.component.sass']
})
export class DialogWindowComponent implements OnInit {

    @Input() text:string;
    @Input() open:boolean;
    @Output() accepted = new EventEmitter<boolean>();

    public timer;
    constructor() { }

    ngOnInit() {
    }

    public ngOnChanges(changes){
        if(changes.open.currentValue == true){
            this.timer = setTimeout(()=>{
                this.yes();
            }, 5000)
        }
    }

    public yes(){
        try{
            clearTimeout(this.timer);
        }catch{
            
        }
        this.open = false;
        this.accepted.emit(true);
    }
}
