import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl,Validators, FormGroup, AbstractControl } from '@angular/forms';
import { Router, NavigationExtras, RoutesRecognized, ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../services/auth.service'
import { AngularTokenService } from 'angular-token';
import { ApiService } from '../../services/api.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-register-page',
    templateUrl: './register-page.component.html',
    styleUrls: ['./register-page.component.sass']
})
export class RegisterPageComponent implements OnInit {
    public isRegistered:boolean = false;
    public isFailed:boolean = false;
    public complexForm : FormGroup;

    constructor(
        private authToken: AngularTokenService,
        private el: ElementRef,
        private router:Router,
        public authService:AuthService,
        public fb: FormBuilder,
    ) { 
        this.complexForm = fb.group({
            'name': ['', Validators.required ],
            'last_name': ['', Validators.required ],
            'email': ['', Validators.required ],
            'password': ['', [Validators.required, Validators.minLength(8)] ],
            'confirm_password': ['', [Validators.required] ],
            'confirmedAccess': [false, Validators.compose([Validators.requiredTrue ])]
        },{
            validator: this.checkPasswords
        });
    }

    ngOnInit() {
    }

    public onSubmit(cf){
        if(cf['confirmedAccess']){
            let data = {
                name: cf['name'],
                last_name: cf['last_name'],
                password: cf['password'],
                email: cf['email'] 
            };

            this.authService.registerUser(data).subscribe(
                res => {
                    console.log(res);
                    if(res){
                        this.isRegistered = true;
                    }else{
                        this.isFailed = true;       
                    }
                }
            );
            // if(this.authService.registerUser(data) == false){
            // }else{
            // }
        }
    }

    public redirect(){
        this.isRegistered = false;
        this.router.navigate(['/']);
    }

    public confirm(e){
        this.complexForm.patchValue({
            'confirmedAccess': e['confirmedAccess'].value
        });
    }


    checkPasswords(control: AbstractControl) {
        let pass = control.get('password').value;
        let confirmPass = control.get('confirm_password').value;
        if(pass !== confirmPass){
            control.get('confirm_password').setErrors({ NoPassswordMatch: true });
        }    
    }    
}
