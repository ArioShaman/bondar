import { 
    Component, 
    OnInit, 
    ViewChild, 
    ElementRef, 
    Input, 
    Output,
    EventEmitter,
    HostListener,

} from '@angular/core';

@Component({
  selector: 'finput-select',
  templateUrl: './form-input-select.component.html',
  styleUrls: ['./form-input-select.component.sass']
})
export class FormInputSelectComponent implements OnInit {

    @ViewChild("myInp") inputEl: ElementRef;
    
    @Input('placeholder') placeholder:string;
    @Input('type') type:string;
    @Input('name') name:string;
    @Input('field') field:string;
    @Input('fontSize') fontSize:string = '18';
    @Input('attributes') attributes:Array<Object> = [];
    @Input('action') action:string = '';

    @Input('data') data:string;
    @Output() dataChange:EventEmitter<Object> = new EventEmitter<Object>();


    public activeAttributes:Array<Object>;
    public activeAttr:Object = {};

    public openedHelper:boolean = false;
    public isFocused:boolean = false;

    public curInd = 0;
    public lastInd = 0;

    constructor(
        private el: ElementRef
    ) { }


    ngOnInit() {
        this.lastInd = this.attributes.length;
        this.activeAttributes = this.attributes.map(a => Object.assign({}, a));
    }
    ngOnChanges(){
        if(this.action == 'focus'){
            this.inputEl.nativeElement.focus(document.querySelector('input'));    
        }
    }

    public stop(e){
        // e.stopPropagation();
    }
    public focusInput(e){   
        this.isFocused = true;
        this.openedHelper = true;
        this.inputEl.nativeElement.focus(document.querySelector('input'));
    }

    ngDoCheck(){
        if(this.data[this.field].length > 0){
            this.isFocused = true;
        }        
    }

    public unfocus(){
        // console.log(this.data);
        this.openedHelper = false;
        if(this.data[this.field].length == 0){
            this.isFocused = false;
        }
    }

    updateList(){
        this.activeAttributes = [];
        for(let attr of this.attributes){
            if(attr['name'].toLowerCase().includes(this.data[this.field].toLowerCase())){
                attr['active'] = false;
                this.activeAttributes.push(attr);
            }
        }
        if(this.activeAttributes.length > 0){
            this.curInd = 1;
            this.activeAttributes[0]['active'] = true;
            this.activeAttr = this.activeAttributes[0];
        }
        this.lastInd = this.activeAttributes.length;        
    }
    public dataChanged(){
        this.dataChange.emit(this.data);
        this.updateList();
    }

    public selectAttribute(attr){
        this.data[this.field] = attr['name'];
        this.dataChanged();
    }


    public onKey(event){
        if(event.code == "ArrowDown"){
            if(this.curInd < this.lastInd){
                this.curInd++;
                if(this.curInd > 1){
                    this.activeAttributes[this.curInd - 2]['active'] = false;
                }
            }else{
                this.curInd = 1;
                this.activeAttributes[this.lastInd - 1]['active'] = false;
            }
            this.activeAttributes[this.curInd - 1]['active'] = true;
            this.activeAttr = this.activeAttributes[this.curInd - 1];             
        }
        if(event.code == "ArrowUp"){
            if(this.curInd > 1){
                this.curInd--;
                this.activeAttributes[this.curInd]['active'] = false;
            }else{
                this.curInd = this.lastInd;
                this.activeAttributes[0]['active'] = false;
            }
            this.activeAttributes[this.curInd - 1]['active'] = true;
            this.activeAttr = this.activeAttributes[this.curInd - 1];
        }
        if(event.code == "Enter" && this.activeAttr['name']){
            this.updateList();
            this.data[this.field] = this.activeAttr['name'];
            this.activeAttr['active'] = false;
        }
    }
}
