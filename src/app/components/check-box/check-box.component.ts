import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'check-box',
    templateUrl: './check-box.component.html',
    styleUrls: ['./check-box.component.sass']
})
export class CheckBoxComponent implements OnInit {
    @Input() state:boolean;
    @Output('update') changeState:EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor() { }

    ngOnInit() {
    
    }

    public change(){
        this.state = this.state ? false : true;
        // console.log(this.state);
        this.changeState.emit(this.state);
    }

}
