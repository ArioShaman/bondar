import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Shoes } from '../../models/shoes'
import { Router, NavigationExtras } from "@angular/router";
import { ApiService } from '../../services/api.service';
import { AngularTokenService } from "angular-token";
import { AuthService } from '../../services/auth.service';
import { environment } from '../../../environments/environment'
var Jimp = require('jimp');


@Component({
    selector: 'shoes-el',
    templateUrl: './shoes-el.component.html',
    styleUrls: ['./shoes-el.component.sass']
})
export class ShoesElComponent implements OnInit {
    @Input() el:Shoes;
    @Input() mobileMinicatalog:boolean = false; 
    @Output() myEvent = new EventEmitter<string>();
    @Output() deleted = new EventEmitter<number>();


    public isDeleting:boolean = false;
    public activeImage;
    public opened:boolean = false;
    public animTimer;
    public markers:Array<Object> = [];
    public saledPrice;
    public host = environment.host;

    public isDarkness:boolean = false;
    public isLoading:boolean = true;

    constructor(
        private router: Router,
        private api: ApiService,
        public tokenAuthService:AngularTokenService        
    ) { }

    public checkPixel(){
        let url = this.host + this.activeImage;

        // Jimp.read(url, (err, img) => {
        //     if (err) throw err;
        //     img = img.resize(256, 256);

        //     var rgb = Jimp.intToRGBA(img.getPixelColor(5, 5))

        //     if(rgb.r < 100 && rgb.b < 100 && rgb.g < 100){
        //         this.isDarkness = true;
        //     }
        // });
            this.isLoading = false;
    }

    ngOnInit() {
        this.activeImage = this.el['images'][0];
        this.checkPixel();

        this.markers = [];
        let el = this.el;
        if(el['isNew']){
            this.markers.push({
                type: 'new',
                text: 'NEW'
            });
        }
        if(el['isSale']){
            this.markers.push({
                type: 'sales',
                text: '-'+el['sale'] + '%' 
            });
        }
        if(el['popular']){
            this.markers.push({
                type: 'popular',
                text: 'TOP'
            });
        }

        this.saledPrice = parseInt(el['price']) - Math.round((parseInt(el['price']) * el['sale'])/100) 
    }

    ngOnChanges(){
        this.activeImage = this.el['images'][0];
    }
    public navigate(el){

        let navigationExtras: NavigationExtras = {
            queryParams: {
                el: JSON.stringify(el)
            }
        };

        this.router.navigate(["/catalog", el.id], navigationExtras);
    }
    public openFooter(){
        this.opened = this.opened ? false : true;
    }

    public clearOpen(e){
        this.opened = false;
        clearInterval(this.animTimer);
        this.activeImage = this.el.images[0]
    }

    public activateColor(color){
        this.el.activeColor = color;
    }

    public activateSize(size){
        this.el.activeSize = size;
    }    

    public activate(type){
        this.myEvent.emit(type);
    }
    public animate(e){
        let i = 0;
        let length = this.el['images'].length;
        this.animTimer = setInterval(()=>{
            if(i < length - 1){
                i++;
            }else{
                i = 0;
            }
            this.activeImage = this.el.images[i];
        }, 1500); 
    }
    public delete(id){
        this.deleted.emit(id);
    }
}
