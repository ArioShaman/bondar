import { Component, OnInit } from '@angular/core';
import { PageShareService } from '../../services/page-share.service';
import { Router, ActivatedRoute } from '@angular/router'; 

@Component({
    selector: 'more-page-mobile',
    templateUrl: './more-page-mobile.component.html',
    styleUrls: ['./more-page-mobile.component.sass']
})
export class MorePageMobileComponent implements OnInit {
    pageState: String;
    constructor(
        public pageService: PageShareService,
        public actRouter: ActivatedRoute,
    ) {
        this.pageState = actRouter.data['value'].page;
        this.pageService.changePage(this.pageState);
    }

    ngOnInit() {
    }

}
