import { Component, OnInit } from '@angular/core';
import { BusketService } from '../../services/busket.service'
import { environment } from '../../../environments/environment';
@Component({
    selector: 'basket',
    templateUrl: './basket.component.html',
    styleUrls: ['./basket.component.sass']
})
export class BasketComponent implements OnInit {
    public orders = [];
    public host = environment.host;
    public totalPrice:number = 0;
    public saledPrice:number = 0;
    public isSale:boolean = false;
    constructor(
        public busketService: BusketService
    ) { }

    ngOnInit() {
        this.busketService.cancelCall();
        this.orders = this.busketService.get();
        this.calculate();
    }

    public calculateTotalPrice(){
        this.totalPrice = 0;
        for(let order of this.orders){
            this.totalPrice += order.count * order.object.price;
        }
    }

    public calculateSaledPrice(){
        this.saledPrice = 0;
        this.isSale = false;
        for(let order of this.orders){
            if(order.object.isSale){
                this.isSale = true;
                this.saledPrice += order.count * order.object.priceSale;
            }else{
                this.saledPrice += order.count * order.object.price;  
            }
        }        
    }

    public decrement(order){
        this.busketService.decrement(order);
        this.calculate();
    }

    public increment(order){
        this.busketService.increment(order);
        this.calculate();
    }

    public deleteOrder(order){
        this.busketService.deleteOrder(order);
        this.calculate();
    }

    // public callModal(){
    //     this.busketService.callMoadal()
    // }
    public calculate(){
        this.calculateTotalPrice();
        this.calculateSaledPrice();        
    }
}
