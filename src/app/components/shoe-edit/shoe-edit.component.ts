import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, RoutesRecognized, ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../services/auth.service'
import { AngularTokenService } from 'angular-token';
import { ApiService } from '../../services/api.service';
import { HelperService } from '../../services/helper.service'
import { FormBuilder, FormControl,Validators, FormGroup } from '@angular/forms';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'shoe-edit',
  templateUrl: './shoe-edit.component.html',
  styleUrls: ['./shoe-edit.component.sass', '../shoe-creator/shoe-creator.component.sass']
})
export class ShoeEditComponent implements OnInit {
    public host = environment.host;
    public id:number;
    public el;
    public indexes:any = [];
    public next:number;
    public last:number;
    public Object = Object;
    public openedParams:boolean = false;
    public isDeleting:boolean = false;
    public isUpdated:boolean = false;
    public openTable:boolean = false;
    public markers:Array<Object> = [
        {
            type: 'new',
            text: 'NEW',
            active: false,
        },
        {
            type: 'sales',
            text:  0,
            active: false, 
        },
        {
            type: 'popular',
            text: 'TOP',
            active: false,          
        }
    ];

    public openedColorPicker:boolean = false;
    public openedSizePicker:boolean = false;

    public attributes:Array<Object> = [
        {
            name: 'Торговая марка',
            active: false
        },
        {
            name: 'Материал верха',
            active: false
        },
        {
            name: 'Материал подкладки',
            active: false
        },
        {
            name: 'Материал стельки',
            active: false
        },
        {
            name: 'Обувная застежка',
            active: false
        }
    ];

    public types = [
        {
            id: 1,
            name: 'Кеды',
            value: 'sneakers'
        },
        {
            id: 2,
            name: 'Кроcсовки',
            value: 'gym shoe'
        },     
        {
            id: 3,
            name: 'Туфли',
            value: 'low shoe'
        },            
    ];

    public checkType(value){
        let typeId = 1;
        for(let type of this.types){
            if(type.value == value){
                typeId = type.id;
            }
        }
        return typeId;
    }
    public action = '';

    public complexForm : FormGroup;

    public colors:any = [];
    public sizes:any = [];

    public newAttribute:Object = {
        key: '',
        value: ''
    };
    public addedAttributes:Array<Object> = [];
    public loadedFiles:Array<Object> = [];

    public zoomStyles = {
        'transform-origin': '0px 0px',
    }


    public activeImage:Object;

    public smallSliderConfig = {
        arrows: false,
        dots: false,
        slidesToShow: 3,
        slidsToScroll: 1,
        centerMode: true,
        focusOnSelect: true,        

    }

    public afterChange(e){
        let index = e.currentSlide;
        this.activeImage = this.complexForm.value.images[index];
    }

    constructor(
        public router:Router,
        public tokenService:AngularTokenService,
        private api: ApiService,
        public _: HelperService,  
        public fb: FormBuilder,
        public actRouter: ActivatedRoute,
        public authService:AuthService,
    ) { 
        this.complexForm = fb.group({
            'title': ['', Validators.required ],
            'price': ['', Validators.required ],
            'colors': [[], Validators.required ],
            'sizes':[[], Validators.required ],
            'images':[[], Validators.required ],
            'params': [{}],
            'description': [''],
            'type_id': [ Validators.required ],
            'sale': [0],
            'isSale': [false],
            'isPopular': [false],
            'isNew': [false]
            // 'number': ['',  Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],

        });
    }

    public saleChange(){
        if(this.complexForm.value['sale'] > 0){
            this.complexForm.patchValue({
                'isSale': true
            });
        }else{
            this.complexForm.patchValue({
                'isSale': false
            });
        }
    }

    ngOnChanges(){
        console.log(this.isDeleting);
    }
    ngOnInit() {
        this.api.get('/api/color').subscribe(
            res =>{
                this.colors = res;
            },
            err =>{
                console.log(err);
            }
        );

        this.api.get('/api/size').subscribe(
            res =>{
                this.sizes = res;
            },
            err =>{
                console.log(err);
            }
        );

        if(!this.tokenService.userSignedIn()){
            this.router.navigate(['/']);
        }
        this.actRouter.params.subscribe(params => {
            this.id = parseInt(params.id);
            this.api.get('/api/shoes/'+this.id).subscribe(
                res => {
                    this.el = res;
                    this.activeImage = this.el.images[0];

                 
                    let el = this.el;
                    for(let colorName of el.colors){
                        if(this.getColorByName(colorName)){
                            this.activateColor(this.getColorByName(colorName));
                        }
                    }
                    for(let sizeName of el.sizes){
                        if(this.getSizeByName(sizeName)){
                            this.activateSize(this.getSizeByName(sizeName));
                        }
                    }                    
                    this.complexForm.patchValue({
                        'title': el.name,
                        'price': el.price,
                        'colors': this.colors,
                        'sizes': this.sizes,
                        'images': el.images,
                        'params': el.params,
                        'description': el.desc,
                        'type_id': this.checkType(el.type),
                        'sale': el.sale,
                        'isSale': el.isSale,
                        'isPopular': el.popular,
                        'isNew': el.isNew                     
                    });
                    // console.log(this.complexForm.value);
                }
            );                   
        });
    }
    public changeState(){
        this.openedParams = !this.openedParams;
    }

    public openColorPicker(){
        this.openedColorPicker = !this.openedColorPicker;
        this.openedSizePicker = false;
    }
    public openSizePicker(){
        this.openedColorPicker = false;
        this.openedSizePicker = !this.openedSizePicker;
    }    

    isActive(list){
        let isActivated = false;
        for(let el of list){
            if(el['active']){
                isActivated = true;
            }
        }
        return isActivated;
    }

    clearActive(list){
        for(let el of list){
            el['active'] = false;
        }        
    }

    public getColorByName(name){
        for(let color of this.colors){
            if(color.name == name){
                return color;
            }
        }
        return null;
    }

    public activateColor(color){
        color['active'] = !color['active'];
        if(this.isActive(this.colors)){
            this.complexForm.patchValue({
                'colors': this.colors
            });          
        }else{
            this.complexForm.patchValue({
                'colors': []
            });            
        }
    }
    
    public getSizeByName(name){
        for(let size of this.sizes){
            if(size.name == name){
                return size;
            }
        }
    }

    public activateSize(size){
        size['active'] = !size['active'];
        if(this.isActive(this.sizes)){
            this.complexForm.patchValue({
                'sizes': this.sizes
            });          
        }else{
            this.complexForm.patchValue({
                'sizes': []
            });            
        }
    }    
    
    public addAttribute(){
        if((this.newAttribute['key'].length != 0) && (this.newAttribute['value'].length != 0)){
            this.complexForm.value.params[this.newAttribute['key']] = this.newAttribute['value'];
            this.newAttribute = {
                key: '',
                value: ''                    
            };    
        }
        this.action = 'focus';
    }

    public onSubmit(complexForm) {
        this.api.update("/api/shoes/"+this.id, complexForm).subscribe(
            res =>{
                console.log(res);
                this.isUpdated = true;
                console.log(this.isUpdated);
            },
            err =>{
                console.log(err);
            }
        );
    }
    
    validateFile(file) {
        let name:string = file['name'];
        var ext = name.substring(name.lastIndexOf('.') + 1);
        if (ext.toLowerCase() == 'jpg') {
            return true;
        }else {
            return false;
        }
    }
    public onFileChanged(e){
        let file = e.target.files[0];
        let hashedFile;
        if(this.validateFile(file)){
            let reader = new FileReader();
            const formData = new FormData();
            formData.append("image", file); 
            formData.append("shoe_id", JSON.stringify(this.id));

            this.api.post("/api/images", formData ).subscribe(
                res => {
                    this.complexForm.value.images.push(res['image']['url']);
                    this.activeImage = this.complexForm.value.images[0];        
                },
                err =>{
                    console.log(err);
                }
            );  
        }
    }    

    public deleteParam(attr){
        delete this.complexForm.value.params[attr];     
    }

    public delete(){
        this.api.delete('/api/shoes/'+ this.id).subscribe(
            res => {
                this.router.navigate(['/catalog']);
            }
        );        
    }
    public deleteImg(image){
        const formData = new FormData();
        formData.append("image", image);    
        formData.append("shoe_id", JSON.stringify(this.id));    
        this.api.post("/api/images/delete", formData ).subscribe(
            res => {
            }
        );
        let index = this.complexForm.value.images.indexOf(image);
        if(index != -1){
            this.complexForm.value.images.splice(index, 1);
            this.activeImage = this.complexForm.value.images[0];
        }
    }

    public enterKey(){
        this.onSubmit(this.complexForm.value);
    }
    public activateMarker(marker){
        let index = this.markers.indexOf(marker);
        this.markers[index]['active'] = !this.markers[index]['active']; 
        if(this.markers[index]['type'] == 'new'){
            this.complexForm.patchValue({
                'isNew': this.markers[index]['active']
            })
        } 
        if(this.markers[index]['type'] == 'popular'){
            this.complexForm.patchValue({
                'isPopular': this.markers[index]['active']
            })
        }
    }
}
