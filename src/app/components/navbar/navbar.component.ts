import { Component, OnInit } from '@angular/core';
import { PageShareService } from '../../services/page-share.service';
import { AngularTokenService } from "angular-token";
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router'; 
import { BusketService } from '../../services/busket.service'

@Component({
    selector: 'navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {
    public isClicked:boolean = false;
    public pageState: String;
    constructor(
        public pageService: PageShareService,
        public tokenAuthService:AngularTokenService,
        public authService:AuthService,
        public router:Router,
        public busketService:BusketService
    ) { 
        this.pageState = this.pageService.pageState;
        this.pageService.pageStateChange.subscribe((value) => { 
            this.pageState = value; 
        });
    }

    public navHeight:number = 0;
    public lastScrollPos = window.pageYOffset;
    public curScrollPos:number = 0;
    public fixed:boolean = false;
    public count:number = 0;

    ngOnInit() {
        this.navHeight = document.querySelector('.nav-scroll').clientHeight;

        window.addEventListener('scroll', () => {
            this.curScrollPos =  window.pageYOffset;
            if(this.curScrollPos > this.navHeight){
                this.fixed = true;
            }else{
                this.fixed = false;
            }
        });
        this.count = this.busketService.getCount();
    }

    public clickLogOut(){
        this.isClicked = true;
        this.signOut();
    }
    public signOut(){        
        this.authService.logOutUser().subscribe(() => this.router.navigate(['/']));
    }
}
