import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Options } from 'ng5-slider';

@Component({
    selector: 'ui-slider',
    templateUrl: './ui-slider.component.html',
    styleUrls: ['./ui-slider.component.sass']
})
export class UiSliderComponent implements OnInit {

    @Input() value:Object;
    @Output('update') valueChange = new EventEmitter();


    public options:Options = {
        step: 500,
        floor: 1000,
        ceil: 10000
    };    
    constructor() { }

    ngOnInit() {
    }
    public change(e){
        this.valueChange.emit(this.value);
    }
}
