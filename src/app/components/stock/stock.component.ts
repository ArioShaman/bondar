import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'; 
import { PageShareService } from '../../services/page-share.service';

@Component({
    selector: 'stock',
    templateUrl: './stock.component.html',
    styleUrls: ['./stock.component.sass']
})
export class StockComponent implements OnInit {

    public pageState: String; 
    constructor(
        router: Router,
        actRouter: ActivatedRoute,
        public pageService: PageShareService,
    ){
        this.pageState = actRouter.data['value'].page;
    }
    ngOnInit() {
        this.pageService.changePage(this.pageState);;
        this.pageState = this.pageService.get();        
    }

}
