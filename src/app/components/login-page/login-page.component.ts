import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { environment } from "../../../environments/environment";
import { AuthService } from '../../services/auth.service';
import { AngularTokenService } from "angular-token";
import { Router } from '@angular/router';

@Component({
    selector: 'login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.sass']
})
export class LoginPageComponent implements OnInit {
    @ViewChild("myInp") inputEl: ElementRef;
    public isFailed:boolean = false;
    public isLogined:boolean = false;
    public FormData = {
        login: '',
        password: '',
    }


    constructor(
        private authToken: AngularTokenService,
        private el: ElementRef,
        private router:Router,
        public authService:AuthService,
    ) {
    }

    ngOnInit() {
    }

    public onSubmit(){
        console.log(this.FormData);
        this.authService.logInUser(this.FormData).subscribe(
            res =>{
                this.isLogined = true;
            },
            err =>{
                this.isFailed = true;
            });    
    }

    public redirect(){
        this.isLogined = false;
        this.router.navigate(['/']);
    }

}
