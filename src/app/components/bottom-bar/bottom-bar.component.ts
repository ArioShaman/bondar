import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageShareService } from '../../services/page-share.service';

@Component({
    selector: 'bottom-bar',
    templateUrl: './bottom-bar.component.html',
    styleUrls: ['./bottom-bar.component.sass']
})
export class BottomBarComponent implements OnInit {

    public pageState: String;

    constructor(
        public pageService: PageShareService,
        public router:Router,        
    ) { 
        this.pageState = this.pageService.pageState;
        this.pageService.pageStateChange.subscribe((value) => { 
            this.pageState = value;
        });
    }

    ngOnInit() {
    }

}
