import { 
    Component, 
    OnInit, 
    ViewChild, 
    ElementRef, 
    Input, 
    Output,
    EventEmitter
} from '@angular/core';

@Component({
  selector: 'fcinput',
  templateUrl: './form-control-input.component.html',
  styleUrls: ['./form-control-input.component.sass']
})


export class FormControlInputComponent implements OnInit {
    
    @ViewChild("myInp") inputEl: ElementRef;
    
    @Input('placeholder') placeholder:string;
    @Input('type') type:string;
    @Input('name') name:string;
    @Input('field') field:string;
    @Input('fontSize') fontSize:string = '18';
    @Input('action') action:string = '';
    @Input('required') required:boolean = false;

    @Input('data') data:string;
    @Output() dataChange:EventEmitter<Object> = new EventEmitter<Object>();

    public isFocused:boolean = false;
    public isTouching:boolean = false;

    constructor(
        private el: ElementRef
    ) { }


    ngOnInit() {
        // console.log(this.data[this.field]);
    }
    ngDoCheck(){
        if(this.data[this.field]['value'].length > 0){
            this.isFocused = true;
        }        
    }
    public focusInput(){
        this.isTouching = true;
        this.isFocused = true;
        this.inputEl.nativeElement.focus(document.querySelector('input'));
    }
    public unfocus(){
        this.isTouching = false;
        if(this.data[this.field]['value'].length == 0){
            this.isFocused = false;
        }
    }
    public dataChanged(){
        this.isTouching = true;
        this.dataChange.emit(this.data);
    }
}
