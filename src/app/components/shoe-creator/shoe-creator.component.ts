import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, RoutesRecognized, ActivatedRoute } from '@angular/router'; 
import { AuthService } from '../../services/auth.service'
import { AngularTokenService } from 'angular-token';
import { ApiService } from '../../services/api.service';
import { HelperService } from '../../services/helper.service'
import { FormBuilder, FormControl,Validators, FormGroup } from '@angular/forms';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'shoe-creator',
    templateUrl: './shoe-creator.component.html',
    styleUrls: ['./shoe-creator.component.sass']
})
export class ShoeCreatorComponent implements OnInit {
    public id:number;
    public el;
    public indexes:any = [];
    public next:number;
    public last:number;
    public Object = Object;
    public openedParams:boolean = false;
    public openTable:boolean = false;
    public markers:Array<Object> = [
        {
            type: 'new',
            text: 'NEW',
            active: false,
        },
        {
            type: 'sales',
            text:  0,
            active: false, 
        },
        {
            type: 'popular',
            text: 'TOP',
            active: false,          
        }
    ];

    public openedColorPicker:boolean = false;
    public openedSizePicker:boolean = false;
    public host = environment.host;
    public attributes:Array<Object> = [
        {
            name: 'Торговая марка',
            active: false
        },
        {
            name: 'Материал верха',
            active: false
        },
        {
            name: 'Материал подкладки',
            active: false
        },
        {
            name: 'Материал стельки',
            active: false
        },
        {
            name: 'Обувная застежка',
            active: false
        }
    ];

    public types = [
        {
            id: 1,
            name: 'Кеды',
        },
        {
            id: 2,
            name: 'Кросcовки',
        },     
        {
            id: 3,
            name: 'Туфли',
        },            
    ];

    public action = '';

    public complexForm : FormGroup;

    public colors:any = [];
    public sizes:any = [];

    public newAttribute:Object = {
        key: '',
        value: ''
    };
    public addedAttributes:Array<Object> = [];
    public loadedFiles:Array<Object> = [];

    public zoomStyles = {
        'transform-origin': '0px 0px',
    }


    public activeImage:Object;
    public isUpdated:boolean = false;
    public smallSliderConfig = {
        arrows: false,
        dots: false,
        slidesToShow: 3,
        slidsToScroll: 1,
        centerMode: true,
        focusOnSelect: true,        

    }


    constructor(
        public router:Router,
        private api: ApiService,
        public fb: FormBuilder,
        public tokenService:AngularTokenService,
        public _: HelperService,  
        public actRouter: ActivatedRoute,
        public authService:AuthService,
    ) { 
        this.complexForm = fb.group({
            'title': ['', Validators.required ],
            'price': ['', Validators.required ],
            'colors': [[], Validators.required ],
            'sizes':[[], Validators.required ],
            'images':[[], Validators.required ],
            'params': [[]],
            'description': [''],
            'type_id': [ Validators.required ],
            'sale': [0],
            'isSale': [false],
            'isPopular': [false],
            'isNew': [false]
        });
    }

    public afterChange(e){
        let index = e.currentSlide;
        this.activeImage = this.loadedFiles[index];
    }

    public redirect(){
        this.isUpdated = false;
        this.router.navigate(['/catalog']);
    }
    public saleChange(){
    
        if(this.complexForm.value['sale'] > 0){
            this.complexForm.patchValue({
                'isSale': true
            });
        }else{
            this.complexForm.patchValue({
                'isSale': false
            });
        }
    }
    ngOnInit() {
        this.api.get('/api/color').subscribe(
            res =>{
                this.colors = res;
            },
            err =>{
                console.log(err);
            }
        );

        this.api.get('/api/size').subscribe(
            res =>{
                this.sizes = res;
            },
            err =>{
                console.log(err);
            }
        );

        if(this.tokenService.userSignedIn()){
            let action = this.actRouter.data['value']['action'];
            if(action == 'create'){

            }
            if(action == 'edit'){

            }
        }else{
            this.router.navigate(['/']);
        }
    }

    public marked(el){
        this.markers = [];
        if(el['isNew']){
            this.markers.push({
                type: 'new',
                text: 'NEW'
            });
        }
        if(el['isSale']){
            this.markers.push({
                type: 'sales',
                text: '-'+this.el['sale'] + '%' 
            });
        }
        if(el['popular']){
            this.markers.push({
                type: 'popular',
                text: 'TOP'
            });
        }              
    }
    public changeState(){
        this.openedParams = !this.openedParams;
    }

    public zoomable(e){
        let x = e.offsetX;
        let y = e.offsetY;
        let styles = x + 'px ' + y + 'px'

        this.zoomStyles['transform-origin'] = styles;
    }

    public openColorPicker(){
        this.openedColorPicker = !this.openedColorPicker;
        this.openedSizePicker = false;
    }
    public openSizePicker(){
        this.openedColorPicker = false;
        this.openedSizePicker = !this.openedSizePicker;
    }    

    isActive(list){
        let isActivated = false;
        for(let el of list){
            if(el['active']){
                isActivated = true;
            }
        }
        return isActivated;
    }

    clearActive(list){
        for(let el of list){
            // console.log(el);
            el['active'] = false;
        }        
    }
    public activateColor(color){
        color['active'] = !color['active'];
        if(this.isActive(this.colors)){
            this.complexForm.patchValue({
                'colors': this.colors
            });          
        }else{
            this.complexForm.patchValue({
                'colors': []
            });            
        }
    }
    
    public activateSize(size){
        size['active'] = !size['active'];
        if(this.isActive(this.sizes)){
            this.complexForm.patchValue({
                'sizes': this.sizes
            });          
        }else{
            this.complexForm.patchValue({
                'sizes': []
            });            
        }
    }    
    
    public addAttribute(){
        if((this.newAttribute['key'].length != 0) && (this.newAttribute['value'].length != 0)){
            this.addedAttributes.push(this.newAttribute);
            this.complexForm.patchValue({
                'params': this.addedAttributes
            });
            this.newAttribute = {
                key: '',
                value: ''                    
            };    
        }
        this.action = 'focus';
    }

    public onSubmit(complexForm) {
        this.api.post("/api/shoes", complexForm).subscribe(
            res =>{
                this.isUpdated = true;
            },
            err =>{
                console.log(err);
            }
        );
    }
    
    validateFile(file) {
        let name:string = file['name'];
        var ext = name.substring(name.lastIndexOf('.') + 1);
        if (ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'png') {
            return true;
        }else {
            return false;
        }
    }
    public onFileChanged(e){
        let file = e.target.files[0];
        let hashedFile;
        if(this.validateFile(file)){
            let reader = new FileReader();
            const formData = new FormData();
            formData.append("image", file);          
            // console.log(formData);
            this.api.post("/api/images", formData ).subscribe(
                res => {
                    this.loadedFiles.push(res);
                    this.activeImage = res;        
                    this.complexForm.patchValue({
                        'images': this.loadedFiles
                    });
                },
                err =>{
                    console.log(err);
                }
            );  
        }
    }    

    public deleteParam(attr){
        let index = this.addedAttributes.indexOf(attr);
        if(index != -1){
            this.addedAttributes.splice(index, 1);
        }
        this.complexForm.patchValue({
            'params': this.addedAttributes
        });        
    }


    public deleteImg(image){
        let index = this.loadedFiles.indexOf(image);
        if(index != -1){
            this.loadedFiles.splice(index, 1);
            this.activeImage = {};
            this.complexForm.patchValue({
                'images': this.loadedFiles
            });              
        }
    }

    public enterKey(){
        this.onSubmit(this.complexForm.value);
    }
    public activateMarker(marker){
        let index = this.markers.indexOf(marker);
        this.markers[index]['active'] = !this.markers[index]['active']; 
        if(this.markers[index]['type'] == 'new'){
            this.complexForm.patchValue({
                'isNew': this.markers[index]['active']
            })
        } 
        if(this.markers[index]['type'] == 'popular'){
            this.complexForm.patchValue({
                'isPopular': this.markers[index]['active']
            })
        }
    }
}
