import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'colors',
    templateUrl: './colors.component.html',
    styleUrls: ['./colors.component.sass']
})
export class ColorsComponent implements OnInit {

    @Input() activatedColors:Array<any>;
    @Output('update') changeActivatedColors:EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

    public colors = [ 
        {
            name: 'black',
            active: false
        },
        {
            name: 'red',
            active: false
        },
        {
            name: 'white',
            active: false
        },
        {
            name: 'bej',
            active: false
        },
        {
            name: 'violet',
            active: false
        },
        {
            name: 'blue',
            active: false
        }
       ];
    constructor() { }

    ngOnInit() {
    }

    public activate(color){
        color['active'] = color['active'] ? false : true; 
        if(color['active']){ 
            this.activatedColors.push(color);
        }
        else{
            let index = this.activatedColors.indexOf(color);
            if(index > -1){
                this.activatedColors.splice(index, 1);
            }
        }
        this.changeActivatedColors.emit(this.activatedColors);
    }
}
