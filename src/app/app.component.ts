import { Component } from '@angular/core';
import { Router, ActivatedRoute, RouterOutlet } from '@angular/router'; 
import { AuthService } from './services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.sass']
})


export class AppComponent {
    public fixed:boolean = false;
    public navHeight:number = 0;
    public deviceWidth:number;
    public loaded = false;//на продакшене поставить false

    constructor(
        authService:AuthService
    ){
        setTimeout(()=>{
            if(document.readyState === "complete"){
                this.loaded = true;
                this.navHeight = document.querySelector('.background').clientHeight;
                window.addEventListener('scroll', () => {
                    let curScrollPos =  window.pageYOffset;
                    this.deviceWidth = window.innerWidth;
                    if(this.deviceWidth > 900){
                        if(curScrollPos > this.navHeight){
                            this.fixed = true;
                        }else{
                            this.fixed = false;
                        }
                    }
                });                 
            }else{
                this.timer();
            }
            console.log(this.loaded);
        }, 3000);        
    }
    public  timer(){
        setTimeout(()=>{
            if(document.readyState === "complete"){
                this.loaded = true;
            }else{
                this.timer();
            }            
        },100);
    }    
    ngOnInit(){
        this.deviceWidth = window.innerWidth;
        console.log(this.deviceWidth);           
    }
}

