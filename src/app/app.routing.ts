import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { TovarCardComponent } from './components/tovar-card/tovar-card.component';
import { CatalogComponent } from './components/catalog/catalog.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StockComponent } from './components/stock/stock.component';
import { PartnersComponent } from './components/partners/partners.component';
import { BasketComponent } from './components/basket/basket.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { ShoeShowComponent } from './components/shoe-show/shoe-show.component';
import { ShoeCreatorComponent } from './components/shoe-creator/shoe-creator.component';
import { ShoeEditComponent } from './components/shoe-edit/shoe-edit.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { OrderPageComponent } from './components/order-page/order-page.component'
import { ProfilePageComponent } from './components/profile-page/profile-page.component'
import { MorePageMobileComponent } from './components/more-page-mobile/more-page-mobile.component';

const appRoutes: Routes = [
    { 
        path: '', component: MainComponent, pathMatch: 'full',
        data: {
            page: 'main'
        }
    },
    { 
        path: 'catalog', component: CatalogComponent ,
        data: {
            page: 'catalog'
        }        
    },
    { 
        path: 'stock', component: StockComponent,
        data: {
            page: 'stock'
        }        
    },
    { 
        path: 'about', component: PartnersComponent,
        data: {
            page: 'partners'
        }        
    },
    { 
        path: 'logIn', component: LoginPageComponent,
        data: {
            page: 'login'
        }        
    },
    { 
        path: 'register', component: RegisterPageComponent,
        data: {
            page: 'register'
        }        
    },    
    { 
        path: 'basket', component: BasketComponent,
        data: {
            page: 'basket'
        }         
    },
    {
        path: 'catalog/:id', component: ShoeShowComponent,
        data: {
            page: 'catalog'
        }
    },
    { 
        path: 'create', component: ShoeCreatorComponent,
        data: {
            page: 'create-shoe',
            action: 'create',
        }         
    }, 
    { 
        path: 'catalog/:id/edit', component: ShoeEditComponent,
        data: {
            page: 'edit-shoe',
            action: 'edit',
        }         
    },
    {
        path: 'profile',
        component: ProfilePageComponent,
        data: {
            page: 'profile'
        }        
    },
    {
        path: 'more',
        component: MorePageMobileComponent,
        data: {
            page: 'more'
        }        
    },
    {
        path: 'ordering',
        component: OrderPageComponent,
        data: {
            page: 'ordering'
        }        
    },     
    {
        path: '404',
        component: NotFoundComponent,
    },
    { path: '**', component: NotFoundComponent }
];


export const AppRouting =  RouterModule.forRoot(
    appRoutes, { enableTracing: false } 
);