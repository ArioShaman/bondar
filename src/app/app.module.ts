import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { AppRouting } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContenteditableModule } from 'ng-contenteditable';

//myservices
import { PageShareService } from './services/page-share.service';
import { HelperService } from './services/helper.service';
import { ShareItemService } from './services/share-item.service';
import { ApiService } from './services/api.service';
import { AuthService } from './services/auth.service';
import { BusketService } from './services/busket.service'
 
//libraries
import { SlickModule } from 'ngx-slick';
import { Ng5SliderModule } from 'ng5-slider';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { AngularTokenService, AngularTokenOptions, AngularTokenModule } from 'angular-token';
import { ClickOutsideModule } from 'ng-click-outside';
import { Ng2CacheModule } from 'ng2-cache';
//pipes
import { FilterPipe } from './pipes/filterPipe';
import { atrFilterPipe } from './pipes/atrFilterPipe';
import { PriceSortPipe } from './pipes/priceSortPipe'


import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { TovarCardComponent } from './components/tovar-card/tovar-card.component';
import { CatalogComponent } from './components/catalog/catalog.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StockComponent } from './components/stock/stock.component';
import { PartnersComponent } from './components/partners/partners.component';
import { BasketComponent } from './components/basket/basket.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { ShoesElComponent } from './components/shoes-el/shoes-el.component';
import { FeedbackComponent } from './components/feedback/feedback.component';

import { TeamSliderComponent } from './components/team-slider/team-slider.component';
import { FooterComponent } from './components/footer/footer.component';
import { CheckBoxComponent } from './components/check-box/check-box.component';
import { ColorsComponent } from './components/colors/colors.component';
import { SizesComponent } from './components/sizes/sizes.component';
import { UiSliderComponent } from './components/ui-slider/ui-slider.component';
import { ShoeShowComponent } from './components/shoe-show/shoe-show.component';
import { BondarInputComponent } from './components/bondar-input/bondar-input.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { ShoeCreatorComponent } from './components/shoe-creator/shoe-creator.component';
import { FormControlInputComponent } from './components/form-control-input/form-control-input.component';
import { FormInputSelectComponent } from './components/form-input-select/form-input-select.component';
import { ControllCheckBoxComponent } from './components/controll-check-box/controll-check-box.component';
import { ShoeEditComponent } from './components/shoe-edit/shoe-edit.component';
import { ModalWindowComponent } from './components/modal-window/modal-window.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UncreatedComponent } from './components/uncreated/uncreated.component';
import { InDevelopComponent } from './components/in-develop/in-develop.component';
import { DialogWindowComponent } from './components/dialog-window/dialog-window.component';
import { OrderPageComponent } from './components/order-page/order-page.component';
import { SizeTableComponent } from './components/size-table/size-table.component';
import { BottomBarComponent } from './components/bottom-bar/bottom-bar.component';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { MorePageMobileComponent } from './components/more-page-mobile/more-page-mobile.component';
import { environment } from '../environments/environment';

import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
import { CreateArticleComponent } from './components/create-article/create-article.component';
// import {SingleMediaPlayer} from './single-media-player';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        TovarCardComponent,
        CatalogComponent,
        NavbarComponent,
        StockComponent,
        PartnersComponent,
        BasketComponent,
        LoginPageComponent,
        ShoesElComponent,
        FeedbackComponent,
        TeamSliderComponent,
        FooterComponent,
        CheckBoxComponent,
        ColorsComponent,
        SizesComponent,
        UiSliderComponent,
        FilterPipe,
        atrFilterPipe,
        PriceSortPipe,
        ShoeShowComponent,
        BondarInputComponent,
        RegisterPageComponent,
        ShoeCreatorComponent,
        FormControlInputComponent,
        FormInputSelectComponent,
        ControllCheckBoxComponent,
        ShoeEditComponent,
        ModalWindowComponent,
        NotFoundComponent,
        UncreatedComponent,
        InDevelopComponent,
        DialogWindowComponent,
        OrderPageComponent,
        SizeTableComponent,
        BottomBarComponent,
        ProfilePageComponent,
        MorePageMobileComponent,
        CreateArticleComponent,
    ],
    imports: [
        BrowserModule.withServerTransition({appId: 'my-app'}),
        HttpClientModule,
        HttpModule,
        AppRouting,
        TransferHttpCacheModule,
        SlickModule.forRoot(),
        Ng5SliderModule,
        CurrencyMaskModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        MatButtonModule, 
        MatCheckboxModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        ContenteditableModule,
        AngularTokenModule.forRoot({
            apiBase: environment.token_auth_config.apiBase           
        }),
        ClickOutsideModule,
        Ng2CacheModule,
        VgCoreModule,
        VgControlsModule,
        VgOverlayPlayModule,
        VgBufferingModule        
    ],
    providers: [
        PageShareService,
        HelperService,
        ShareItemService,
        ApiService,
        AngularTokenService,
        AuthService,
        BusketService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
