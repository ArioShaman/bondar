export const tableSizes:Array<Object> = [
    {
        size: '37',
        len: '23'
    },
    {
        size: '38',
        len: '23.5'
    },
    {
        size: '39',
        len: '23.5'
    },
    {
        size: '40',
        len: '24 - 25'
    },
    {
        size: '41',
        len: '25'
    },
    {
        size: '42',
        len: '26'
    },
    {
        size: '43',
        len: '26.5'
    },
    {
        size: '44',
        len: '27'
    },
    {
        size: '45',
        len: '28'
    },  
    {
        size: '46',
        len: '29'
    },  
    {
        size: '47',
        len: '30'
    },  
    {
        size: '48',
        len: '31'
    },                                                                                        
];