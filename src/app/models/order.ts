import { Shoes } from './shoes';

export class Order{
    public count: number;
    public object: Object;
    constructor(
        count, object
    ){
        this.count = count;
        this.object = object;
    }
}