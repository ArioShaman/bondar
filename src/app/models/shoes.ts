export class Shoes
{
    public id: number;
    public name: string;
    public images: Array<string>;
    public colors: Array<string>;
    public sizes: Array<string>;
    public activeColor:string;
    public activeSize:string;
    public price: string;
    public type: string;
    public isNew: boolean;
    public isSale: boolean;
    public sale: number;
    public popular: boolean;
    public rating: number;
}