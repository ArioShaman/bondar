import { Shoes } from './shoes';

export const shoesList:Array<Shoes> = [
    {
        id: 1, 
        name: 'Nike ada 34',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black',  'red', 'white', 'bej', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '1499',
        type: 'sneakers', 
        isNew: true,
        isSale: true,
        sale: 40,
        popular: true,
        rating: 0,   
    },
    {
        id: 2, 
        name: 'Nike pile 22',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '2199',
        type: 'sneakers',
        isNew: true,
        isSale: false,
        sale: 0,
        popular: true,
        rating: 0,          
    },
    {
        id: 3, 
        name: 'Nike Peterson',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white'
        ],
        sizes: [
            '41', '42'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '3999',        
        type: 'sneakers',
        isNew: true,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,          
    },
    {
        id: 4, 
        name: 'Nike Valid 3000',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '1899',
        type: 'sneakers',   
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,            
    },
    {
        id: 5, 
        name: 'Nike hren vam a ne nike',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '1649',
        type: 'sneakers',
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,                  
    },
    {
        id: 6, 
        name: 'Nike bomb site',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',  
        price: '5299',   
        type: 'sneakers',
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,             
    }, 
    {
        id: 7, 
        name: 'Nike 44 BOOM',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '4099', 
        type: 'gym shoe',
        isNew: true,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,                 
    }, 
    {
        id: 8, 
        name: 'Nike а по русски',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',   
        price: '1499',
        type: 'sneakers',
        isNew: true,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,               
    },    
    {
        id: 9, 
        name: 'Nike hren vam a ne nike',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '1649',  
        type: 'sneakers',   
        isNew: true,
        isSale: true,
        sale: 30,
        popular: false,
        rating: 0,             
    },
    {
        id: 10, 
        name: 'Nike bomb site',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',  
        price: '5299',  
        type: 'gym shoe',  
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,            
    }, 
    {
        id: 11, 
        name: 'Nike 44 BOOM',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '4099',
        type: 'sneakers',   
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,               
    }, 
    {
        id: 12, 
        name: 'Nike а по русски',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',   
        price: '1499', 
        type: 'sneakers',
        isNew: false,
        isSale: true,
        sale: 10,
        popular: true,
        rating: 0,              
    },  
    {
        id: 13, 
        name: 'Nike bomb site',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',  
        price: '5299',  
        type: 'gym shoe',  
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,            
    }, 
    {
        id: 14, 
        name: 'Nike 44 BOOM',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '4099',
        type: 'sneakers',   
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,               
    }, 
    {
        id: 15, 
        name: 'Nike а по русски',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',   
        price: '1499', 
        type: 'sneakers',
        isNew: false,
        isSale: true,
        sale: 10,
        popular: true,
        rating: 0,              
    },   
    {
        id: 16, 
        name: 'Nike bomb site',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',  
        price: '5299',  
        type: 'gym shoe',  
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,            
    }, 
    {
        id: 17, 
        name: 'Nike 44 BOOM',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '4099',
        type: 'sneakers',   
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,               
    }, 
    {
        id: 18, 
        name: 'Nike а по русски',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',   
        price: '1499', 
        type: 'sneakers',
        isNew: false,
        isSale: true,
        sale: 10,
        popular: true,
        rating: 0,              
    },         
    {
        id: 19, 
        name: 'Nike bomb site',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',  
        price: '5299',  
        type: 'gym shoe',  
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,            
    }, 
    {
        id: 20, 
        name: 'Nike 44 BOOM',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '4099',
        type: 'sneakers',   
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,               
    }, 
    {
        id: 21, 
        name: 'Nike а по русски',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',   
        price: '1499', 
        type: 'sneakers',
        isNew: false,
        isSale: true,
        sale: 10,
        popular: true,
        rating: 0,              
    },       
    {
        id: 22, 
        name: 'Nike bomb site',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',  
        price: '5299',  
        type: 'gym shoe',  
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,            
    }, 
    {
        id: 23, 
        name: 'Nike 44 BOOM',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',
        price: '4099',
        type: 'sneakers',   
        isNew: false,
        isSale: false,
        sale: 0,
        popular: false,
        rating: 0,               
    }, 
    {
        id: 24, 
        name: 'Nike а по русски',
        images:  [
            '1.png', 
            '2.png', 
            '3.png',
        ],
        colors: [
            'black', 'white', 'bej', 'red'
        ],
        sizes: [
            '41', '42', '43', '45', '47', '48'
        ],
        activeColor: 'black',
        activeSize: '41',   
        price: '1499', 
        type: 'sneakers',
        isNew: false,
        isSale: true,
        sale: 10,
        popular: true,
        rating: 0,              
    },                               
];
