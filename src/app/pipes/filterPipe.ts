import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})


export class FilterPipe implements PipeTransform {
    public contains(values, array){
        if(values.length > 0){
            for(let item of values){
                if(array.includes(item['name'])){
                    return true;
                }
            }
            return false;
        }else{
            return true;
        }
    }
    public isInPriceInterval(item, minValue, maxValue){
        if(item['price'] >= minValue && item['price'] <= maxValue){
            return true;
        }else{
            return false;
        }
    }
    transform(items: any, filters: Array<Object>): any{
        var filteredItems = [];

        let colorFilter = filters[0]['value'];
        let sizeFilter =  filters[1]['value'];
        let minPriceFilter = filters[2]['value'];
        let maxPriceFilter = filters[3]['value'];

        items.filter(item => {
            if(
                this.isInPriceInterval(item, minPriceFilter, maxPriceFilter) && 
                (
                    this.contains(colorFilter, item['colors']) &&
                    this.contains(sizeFilter, item['sizes']) 
                )
            ){
                filteredItems.push(item);
            }
        });
        
        return filteredItems;
    }
}