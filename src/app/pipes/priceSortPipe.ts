import { Pipe, PipeTransform } from '@angular/core';
import { HelperService } from './../services/helper.service';

@Pipe({
    name: 'priceSort',
    pure: false
})

export class PriceSortPipe implements PipeTransform {
    constructor( private _: HelperService){

    }   

    transform(items, priceOrder:String) {
        if(priceOrder != ''){
            if(priceOrder == 'asc'){
                items = this._.sortBy(items, 'price');
            }
            if(priceOrder == 'desc'){
                items = this._.sortBy(items, 'price').reverse();
            }
        }
        return items;
    }
}