import { Pipe, PipeTransform } from '@angular/core';
import { HelperService } from './../services/helper.service'

@Pipe({
    name: 'atrFilter',
    pure: false
})

export class atrFilterPipe implements PipeTransform {
    constructor( private _: HelperService){

    }    


    public getLastIn(array, el){
        let last = this._.findLastIndex(array, el)
        console.log(last);
        return last;
    }

    transform(items, orderArgs: Array<Object>) {
        var filteredItems = [];

        let isNewArg = orderArgs[0];
        let isSaleArg = orderArgs[1];
        let isPopularArg = orderArgs[2];
        let sneakersArg = orderArgs[3];
        let gymArg = orderArgs[4];
        let lowArg = orderArgs[5];

        items.filter(item => {
            //single filter
            if(isNewArg['value'] && !isSaleArg['value'] && !isPopularArg['value'] ){
                    if(item['isNew']){
                        filteredItems.push(item);
                    }
            }

            if(!isNewArg['value'] && isSaleArg['value'] && !isPopularArg['value'] ){
                if(item['isSale']){
                    filteredItems.push(item);
                }
            }
            if(!isNewArg['value'] && !isSaleArg['value'] && isPopularArg['value'] ){
                if(item['popular']){
                    filteredItems.push(item);
                }                      
            }
            //double filter
            if(isNewArg['value'] && isSaleArg['value'] && !isPopularArg['value'] ){
                if(item['isNew'] && item['isSale']){
                    filteredItems.push(item);
                }
            }

            if(!isNewArg['value'] && isSaleArg['value'] && isPopularArg['value'] ){
                if(item['popular'] && item['isSale']){
                    filteredItems.push(item);
                }
            }

            if(isNewArg['value'] && !isSaleArg['value'] && isPopularArg['value'] ){
                if(item['isNew'] && item['popular']){
                    filteredItems.push(item);
                }
            }
            //triable
            if(isNewArg['value'] && isSaleArg['value'] && isPopularArg['value'] ){
                if(item['isNew'] && item['popular'] && item['isSale']){
                    filteredItems.push(item);
                }
            }            

            if(filteredItems.length > 0){
                if(sneakersArg['value']){
                    filteredItems = this._.reject(filteredItems, (item)=>{
                        return item['type'] != 'sneakers';
                    });
                }
                if(gymArg['value']){
                    filteredItems = this._.reject(filteredItems, (item)=>{
                        return item['type'] != 'gym shoe';
                    });                    
                }
                if(lowArg['value']){
                    filteredItems = this._.reject(filteredItems, (item)=>{
                        return item['type'] != 'low shoe';
                    });                    
                }                              
            }else{
                if(sneakersArg['value']){
                    items = this._.reject(items, (item)=>{
                        return item['type'] != 'sneakers';
                    });                    
                }
                if(gymArg['value']){
                    items = this._.reject(items, (item)=>{
                        return item['type'] != 'gym shoe';
                    });                    
                }  
                if(lowArg['value']){
                    items = this._.reject(items, (item)=>{
                        return item['type'] != 'low shoe';
                    });                    
                }                                
            }
        });                

        if(filteredItems.length > 0){
            return filteredItems;
        }else{
            return items;
        }

    }
}