import { Injectable } from '@angular/core';
import { Subject } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class PageShareService {

    public pageState: String;
    public pageStateChange: Subject<String> = new Subject<String>();

    constructor() {
    }

    public get(){
        return this.pageState;
    }

    public changePage(page: String){
        this.pageState = page;
        this.pageStateChange.next(this.pageState);
        console.log(this.pageState);
    }
}
