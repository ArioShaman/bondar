import { Injectable } from '@angular/core';
import { AngularTokenService } from "angular-token";
import { Subject, Observable } from "rxjs";
import { Response } from "@angular/http";
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute, RouterOutlet } from '@angular/router'; 
import { ApiService } from '../services/api.service';
import { of } from 'rxjs';

@Injectable()
export class AuthService {

    userSignedIn$:Subject<boolean> = new Subject();

    constructor(
        public authService:AngularTokenService,
        public router:Router,
        private api: ApiService,
    ) {
        this.authService.validateToken().subscribe(
            res => res.status == 200 ? this.userSignedIn$.next(res.json().success) : this.userSignedIn$.next(false)
        )
  }

    logOutUser():Observable<Response>{

        return this.authService.signOut().pipe(map(
            res => {
                this.userSignedIn$.next(false);
                return res;
            }
        ));
    }

    registerUser(signUpData:  {name:string, last_name:string, password:string, email:string}):Observable<any>{
        let subject = new Subject();
        this.api.post('/api/auth', signUpData).subscribe(
            res => {
                this.logInUser({login: signUpData.email, password: signUpData.password}).subscribe(
                    res => {
                        subject.next(true);
                        subject.complete();
                    },
                    err =>{
                        subject.next(false);
                        subject.complete();
                    }
                );
            },
            err => {
                subject.next(true);
                subject.complete();
            }
        );
        return subject.asObservable();

  }

    logInUser(signInData: {login:string, password:string}):Observable<Response>{
        return this.authService.signIn(signInData).pipe(map(
            res => {
                this.userSignedIn$.next(true);
                // this.router.navigate(['/']);
                return res;
            }
        ));

    }

}