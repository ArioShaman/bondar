import { Injectable } from '@angular/core';
import { _ } from 'underscore';

@Injectable()
export class HelperService {

    constructor() { }

    public each(array, delegate) {
        return _.each(array, delegate);
    }

    public sortBy(list, iteratee){
        return _.sortBy(list, iteratee);
    }
    public chain(obj){
        return _.chain(obj);
    }
    public lastIndexOf(array, value){
        return _.lastIndexOf(array, value);
    }
    public findLastIndex(array, value){
        return _.findLastIndex(array, value);
    }
    public reject(list, predicate){
        return _.reject(list, predicate);
    }

    public isEmpty(object){
        return _.isEmpty(object);
    } 
}