import { Injectable } from '@angular/core';
import { Subject } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class ShareItemService {
    public pagesCount:number;
    public pagesCountChange: Subject<number> = new Subject<number>();

    constructor() { }

    public get(){
        return this.pagesCount;
    }

    public changePageCount(count){
        this.pagesCount = count;
        this.pagesCountChange.next(this.pagesCount);
        console.log(count);
    }
}
