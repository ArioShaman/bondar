import { Injectable } from '@angular/core';
import { Order } from '../models/order';
import { CacheService, CacheStoragesEnum } from 'ng2-cache';
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BusketService {

    public busket:Array<Order> = new Array<Order>();
    public busketStateChange: Subject<Array<Order>> = new Subject<Array<Order>>();

    public call: boolean = false;
    public callChange: Subject<boolean> = new Subject<boolean>();
    public selectedOrder:Order;
    public quickOrder:Order;

    public isQuick:boolean = false;
    public count = 0;
    public countChange: Subject<number> = new Subject<number>();




    public callModal(order = this.selectedOrder){
        if(order !=  this.selectedOrder){
            this.selectedOrder = order;
        }
        this.call = true;
        this.callChange.next(this.call);
    }

    constructor(public cache: CacheService) { 
        if(cache.get('busket')){
            if(cache.get('busket').length > 0){
                this.busket = cache.get('busket');
            }
        }else{
            cache.set('busket', []);
        }
        if(cache.get('isQuick')){
            if(cache.get('isQuick') == true){
                this.isQuick = true;
            }
        }else{
            cache.set('isQuick', false);
        }
    }

    public get(){
        return this.busket;
    }

    public getIsQuick(){
        return this.cache.get('isQuick');
    }
    public getCall(){
        return this.call;
    }
    public cancelCall(){
        this.call = false;
    }

    public getCount(){
        this.count = 0;
        for(let order of this.busket){
            this.count += order.count;
        }
        this.countChange.next(this.count);
        return this.count;
    }

    public clear(){
        this.busket = [];
        this.busketStateChange.next(this.busket);
        this.cache.set('busket', this.busket);
    }
    
    public clearQuick(){
        this.isQuick = false;
        this.cache.set('isQuick', this.isQuick);
    }

    public setQuickOrder(shoe){
        this.isQuick = true;
        this.cache.set('isQuick', this.isQuick);

        let order = new Order(1, Object.assign({}, shoe));
        this.quickOrder = order;
        console.log(this.quickOrder);
    }
    public addOrder(shoe){
        let existObject = false;
        if(this.busket.length > 0){
            for(let exOrder of this.busket){
                if(JSON.stringify(exOrder.object) == JSON.stringify(shoe)){
                    existObject = true;
                    exOrder.count++;
                } 
            }
        }
        if(!existObject){
            let order = new Order(1, Object.assign({}, shoe));
            this.busket.push(order);
            this.busketStateChange.next(this.busket);
            this.cache.set('busket', this.busket);
        }
    }
    public deleteOrder(order){
        let index = this.busket.indexOf(order);
        if( index > -1){
            this.busket.splice(index, 1);
            this.busketStateChange.next(this.busket);
            this.cache.set('busket', this.busket);
            this.call = false;
        }
    }
    public decrement(order){
        if(order['count'] > 0){
            if(order['count'] == 1){
                this.selectedOrder = order;
                this.callModal();
            }else{
                let index = this.busket.indexOf(order);
                this.busket[index]['count']--;
                this.busketStateChange.next(this.busket);
                this.cache.set('busket', this.busket);   
            }
        }
    }

    public increment(order){
        let index = this.busket.indexOf(order);
        this.busket[index]['count']++;
        this.busketStateChange.next(this.busket);
        this.cache.set('busket', this.busket);        
    }        
}
