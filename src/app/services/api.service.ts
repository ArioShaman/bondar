import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(public http: HttpClient) {
    }

    API_URL : string = environment.host;
    // API_URL : string = "";

    public headers = new HttpHeaders();

    public get(path) {
        var endpoint = this.API_URL + path;
        return this.http.get(endpoint);
    }
 
    public post(path:string, body:any) {
        var endpoint = this.API_URL + path;
        return this.http.post(endpoint, body);
    }
    
    public delete(path:string){
        var endpoint = this.API_URL + path;
        return this.http.delete(endpoint);
    }  
  
    public update(path:string, body:any){
        var endpoint = this.API_URL + path;
        return this.http.put(endpoint, body);
    }  
}
