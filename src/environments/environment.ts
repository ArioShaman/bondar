// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular.json`.

export const environment = {
    // host: 'http://83.220.175.223:5000',
    host: 'http://bondar.store:5000',
    production: false,
    token_auth_config: {
        apiBase: 'http://bondar.store:5000/api'
    }
};
